package com.smile.cn.bean;

import com.smile.cn.entity.EMail;
import com.smile.cn.entity.EMailAttachment;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.pop3.POP3Folder;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeUtility;

public class SaveEmailBean {

	public EMail saveMessage(Message msg, String account, String mailType,
			Folder folder) throws Exception {
		return handleMessage(msg, account, mailType, folder, false);
	}

	public EMail saveMail(Message msg, String account, String mailType,
			Folder folder) throws Exception {
		return handleMessage(msg, account, mailType, folder, true);
	}

	private EMail handleMessage(Message msg, String username, String mailType,
			Folder folder, boolean saveContent) throws Exception {
		String encoding = "";
		if ((msg.getHeader("Content-Transfer-Encoding") != null)
				&& (msg.getHeader("Content-Transfer-Encoding").length > 0)) {
			encoding = msg.getHeader("Content-Transfer-Encoding")[0];
		}
		EMail mail = new EMail();
		mail.setAccountName(username);
		mail.setNumber(msg.getMessageNumber());
		if ((mailType != null) && (mailType.equalsIgnoreCase("imap"))) {
			IMAPFolder imapFolder = (IMAPFolder) folder;
			mail.setId(String.valueOf(imapFolder.getUID(msg)));
		} else if (mailType != null) {
			POP3Folder pop3Folder = (POP3Folder) folder;
			mail.setId(pop3Folder.getUID(msg));
		}
		String from = getFrom(msg);
		mail.setAddresser(ParseMail.parseAddresser(encoding, from));
		mail.setReceiver(getMailAddress(msg, "TO"));
		mail.setCc(getMailAddress(msg, "CC"));
		mail.setBcc(getMailAddress(msg, "BCC"));
		mail.setReplyTo(ParseMail.parseAddresser(encoding, from));
		mail.setMailTitle(ParseMail.parseMailSubject(encoding, msg.getSubject()));
		if (msg.getSentDate() != null)
			mail.setSendTime(msg.getSentDate());
		else {
			mail.setSendTime(new Date());
		}
		if (saveContent) {
			if ((username.contains("sohu.com"))
					|| (username.contains("21cn.com"))
					|| (username.contains("sogou.com"))) {
				mail.setContent("无法解析邮件内容");
				mail.setAttachment(new ArrayList());
			} else {
				String content = getMailContentForInit(msg);

				mail.setContent(ParseMail.parseContent(content));
				List emailAttachments = this.getAttach(msg);
				mail.setAttachment(emailAttachments);
			}
		}
		return mail;
	}

	private String getFrom(Message msg) throws Exception {
		String personal = ((InternetAddress) msg.getFrom()[0]).getPersonal();
		String address = ((InternetAddress) msg.getFrom()[0]).getAddress();
		if (personal == null) {
			if (address.contains("@"))
				personal = address.substring(0, address.indexOf("@"));
			else
				personal = address;
		} else {
			personal = MimeUtility.decodeText(personal);
		}
		return personal + "<" + address + ">";
	}

	public String getMailContentForInit(Part part) throws Exception {
		StringBuffer bodyText = new StringBuffer();
		String contenttype = part.getContentType();
		int nameindex = contenttype.indexOf("name");
		boolean conname = false;
		if (nameindex != -1) {
			conname = true;
		}
		if ((part.isMimeType("text/plain")) && (!conname)) {
			// if (log.isDebugEnabled()) {
			// log.debug(part.getContent());
			// }
			String text = (String) part.getContent();
			bodyText.append(text);
		} else if ((part.isMimeType("text/html")) && (!conname)) {
			String cont = (String) part.getContent();
			bodyText.append(cont);
		} else if (part.isMimeType("message/rfc822")) {
			getMailContentForInit((Part) part.getContent());
		} else if (part.isMimeType("multipart/*")) {
			Multipart multipart = (Multipart) part.getContent();
			int counts = multipart.getCount();
			for (int i = 0; i < counts; i++) {
				Part part2 = multipart.getBodyPart(i);
				bodyText.append(getMailContentForInit(part2));
			}
		}

		return bodyText.toString();
	}

	private String getContent(Part part) throws Exception {
		String a = "";
		if (part.isMimeType("text/plain")) {
			String s = (String) part.getContent();
			return s;
		}
		if (part.isMimeType("text/html")) {
			String s = (String) part.getContent();
			return s;
		}
		if (part.isMimeType("multipart/*")) {
			StringBuffer sb = new StringBuffer();
			if (part.isMimeType("multipart/alternative")) {
				Multipart mp = (Multipart) part.getContent();
				int index = 0;
				if (mp.getCount() > 1) {
					index = 1;
				}
				Part tmp = mp.getBodyPart(index);
				if (index == 0) {
					sb.append(getContent(tmp));
					return sb.toString();
				}
				if (index == 1) {
					try {
						sb.append(tmp.getContent());
					} catch (Exception e) {
						sb.append(tmp.getContent().toString());
					}
					return sb.toString();
				}
			} else {
				if (part.isMimeType("multipart/related")) {
					Multipart mp = (Multipart) part.getContent();
					Part tmp = mp.getBodyPart(0);
					String body = getContent(tmp);
					return body;
				}
				Multipart mp = (Multipart) part.getContent();
				Part tmp = mp.getBodyPart(0);
				a = getContent(tmp);
			}
			return a;
		}
		if (part.isMimeType("message/rfc822")) {
			String s = getContent((Part) part.getContent());
			return s;
		}
		return part.getContent().toString();
	}

	public List<EMailAttachment> getAttach(Part part) throws Exception {
		List<EMailAttachment> emailAttachments = new ArrayList<EMailAttachment>();
		if (part.isMimeType("multipart/*")) {
			Multipart mp = (Multipart) part.getContent();
			int count = mp.getCount();
			int k = 1;
			do {
				Part mpart = mp.getBodyPart(k);
				String disposition = mpart.getDisposition();
				if ((disposition != null) && (disposition.equals("attachment"))) {
					EMailAttachment emailAttachment = new EMailAttachment();
					if (mpart.getHeader("Content-ID") != null) {
						emailAttachment.setcId(mpart.getHeader("Content-ID")[0]
								.replace("<", "").replace(">", ""));
					}
					String fileName = ParseMail.getMessageFileName(mpart
							.getFileName());
					String contentType = mpart.getContentType().substring(0,
							mpart.getContentType().indexOf(";"));
					if (contentType == null || contentType.equalsIgnoreCase("")) {
						contentType = "未知附件类型";
					}
					if (fileName != null && !fileName.equalsIgnoreCase(""))
						emailAttachment.setFileName(fileName);
					else {
						emailAttachment.setFileName("未知附件名");
					}
					InputStream in = null;
					ByteArrayOutputStream fout = null;
					try {
						in = mpart.getInputStream();
						fout = new ByteArrayOutputStream();
						int ch = 0;
						while ((ch = in.read()) != -1) {
							// int ch;
							fout.write(ch);
						}
					} finally {
						fout.close();
						in.close();
					}
					byte[] data = new byte[0];
					data = fout.toByteArray();
					emailAttachment.setType(contentType);
					emailAttachment.setData(data);
					emailAttachment.setSize(Integer.valueOf(data.length));
					emailAttachments.add(emailAttachment);
				}
				k++;
				if (count <= 1)
					break;
			} while (k < count);
		} else if (part.isMimeType("message/rfc822")) {
			emailAttachments = this.getAttach((Part) part.getContent());
		}
		return emailAttachments;
	}

	public String getMailAddress(Message msg, String type) throws Exception {
		String mailaddr = "";
		String addtype = type.toUpperCase();
		InternetAddress[] address = (InternetAddress[]) null;
		if ((addtype.equals("TO")) || (addtype.equals("CC"))
				|| (addtype.equals("BCC"))) {
			if (addtype.equals("TO"))
				address = (InternetAddress[]) msg
						.getRecipients(Message.RecipientType.TO);
			else if (addtype.equals("CC"))
				address = (InternetAddress[]) msg
						.getRecipients(Message.RecipientType.CC);
			else {
				address = (InternetAddress[]) msg
						.getRecipients(Message.RecipientType.BCC);
			}
			if (address == null) {
				return mailaddr;
			}
			for (int i = 0; i < address.length; i++) {
				String email = address[i].getAddress();
				if (email == null)
					email = "";
				else {
					email = MimeUtility.decodeText(email);
				}
				String personal = address[i].getPersonal();
				if (personal == null)
					personal = "";
				else {
					personal = MimeUtility.decodeText(personal);
				}
				String compositeto = personal + "<" + email + ">";
				mailaddr = mailaddr + ";" + compositeto;
			}
			mailaddr = mailaddr.substring(1);
		}
		return mailaddr;
	}

	public int getReplySign(Message msg) throws MessagingException {
		int replysign = 0;
		String[] needreply = msg.getHeader("Disposition-Notification-To");
		if (needreply != null) {
			replysign = 1;
		}
		return replysign;
	}
}
