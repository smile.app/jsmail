package com.smile.cn.bean;

import com.smile.cn.entity.EMailServer;
import com.smile.cn.entity.MailAuthentication;
import com.smile.cn.entity.config.EMailConfig;
import com.sun.mail.imap.IMAPStore;
import com.sun.mail.util.MailSSLSocketFactory;
import com.sun.net.ssl.internal.ssl.Provider;
import java.security.Security;
import java.util.Properties;
import javax.mail.Folder;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;

public class GetFolderBean {
	//private final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
	private String mailType = null;
	private Store store = null;
	private IMAPStore iStore = null;

	public String getMailType() {
		return this.mailType;
	}

	public Folder getFolder(MailAuthentication account) throws Exception {
		Properties props = new Properties();
		EMailServer server = EMailConfig.getEMailServerByUser(account
				.getUserName());
		this.mailType = server.getMailtype();
		MailSSLSocketFactory sf = null;
		Session session = null;
		URLName urln = null;
		sf = new MailSSLSocketFactory();
		sf.setTrustAllHosts(true);
		props.setProperty("mail." + this.mailType.toLowerCase()
				+ ".starttls.enable", "true");
		props.put("mail." + this.mailType.toLowerCase() + ".ssl.socketFactory",
				sf);
		if (!"none".equalsIgnoreCase(server.getSecurityprotocol())) {
			Security.addProvider(new Provider());
			props.setProperty("mail." + this.mailType.toLowerCase()
					+ ".socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.setProperty("mail." + this.mailType.toLowerCase()
					+ ".socketFactory.fallback", "false");
			if ("tls".equalsIgnoreCase(server.getSecurityprotocol())) {
				props.setProperty("mail." + this.mailType.toLowerCase()
						+ ".port", String.valueOf(server.getPopport()));
				props.setProperty("mail." + this.mailType.toLowerCase()
						+ ".socketFactory.port",
						String.valueOf(server.getPopport()));
			} else {
				props.setProperty("mail." + this.mailType.toLowerCase()
						+ ".port", String.valueOf(server.getImapport()));
				props.setProperty("mail." + this.mailType.toLowerCase()
						+ ".socketFactory.port",
						String.valueOf(server.getImapport()));
			}
		}
		session = Session.getInstance(props, null);
		session.setDebug(true);
		try {
			Folder localFolder1=null;
			if ((this.mailType.equalsIgnoreCase("pop3"))
					|| (this.mailType.equalsIgnoreCase("pop"))) {
				urln = new URLName(this.mailType, server.getPopserver(),
						server.getPopport(), null, account.getUserName(),
						account.getPassword());
			} else {
				urln = new URLName(this.mailType, server.getImapserver(),
						server.getImapport(), null, account.getUserName(),
						account.getPassword());
				this.iStore = ((IMAPStore) session.getStore(urln));
				this.iStore.connect();

				Folder folder = this.iStore.getFolder("INBOX");
				return folder;
			}
			this.store = session.getStore(urln);
			this.store.connect();
			Folder folder = this.store.getFolder("INBOX");
			return folder;
		} catch (Exception e) {
			throw new Exception("connect email server failed");
		} finally {
			if ((this.mailType.equalsIgnoreCase("pop3"))
					|| (this.mailType.equalsIgnoreCase("pop"))) {
				if ((this.store != null) && (this.store.isConnected())) {
					this.store.close();
				}
			} else if ((this.iStore != null) && (this.iStore.isConnected()))
				this.iStore.close();
		}
	}
}