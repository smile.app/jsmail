package com.smile.cn.bean;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.internet.MimeUtility;

public class ParseMail{

  public static Map<String, Object> parseHeaders(Message msg)
    throws Exception
  {
    Enumeration e = msg.getAllHeaders();
    Map headers = new HashMap();
    while (e.hasMoreElements()) {
      Header header = (Header)e.nextElement();
      headers.put(header.getName(), header.getValue());
    }
    return headers;
  }

  public static Date getSearchDate(String date) {
    String[] ymd = date.split("-");

    Date sdate = null;
    try {
      int year = Integer.parseInt(ymd[0]);
      int month = Integer.parseInt(ymd[1]) - 1;
      int day = Integer.parseInt(ymd[2]);
      Calendar cal = Calendar.getInstance();
      cal.set(year, month, day);
      sdate = cal.getTime();
    } catch (Exception ex) {
      //log.error(ex.getMessage());
    }
    return sdate;
  }

  public static String parseDate(Date date){
    SimpleDateFormat formater = new SimpleDateFormat("yyyy年MM月dd日 HH:mm (EEEE)");
    String stime = formater.format(date);
    return stime;
  }

  public static String parseContent(String content) {
    if (content.contains("<head>")) {
      int idx = content.indexOf("<head>");
      int idx1 = content.indexOf("</head>");
      content = content.replace(content.substring(idx, idx1 + 7), "");
      return content;
    }if (content.contains("<HEAD>")) {
      int idx = content.indexOf("<HEAD>");
      int idx1 = content.indexOf("</HEAD>");
      content = content.replace(content.substring(idx, idx1 + 7), "");
    }
    return content;
  }

  public static String parseAddresser(String encoding, String addresser)throws Exception{
    if ((addresser == null) || (addresser.equals(""))) {
      throw new Exception("参数addresser不能为空");
    }
    if ((encoding != null) && (encoding.equalsIgnoreCase("8bit"))) {
      byte[] b = addresser.getBytes();
      return new String(b, "gbk");
    }
    if (addresser.indexOf("=?x-unknown?") >= 0) {
      addresser = addresser.replace("x-unknown", "gbk");
    }
    return MimeUtility.decodeText(addresser);
  }

  public static String parseMailSubject(String encoding, String subject){
    try {
      if (subject != null) {
        if ((encoding != null) && (encoding.equalsIgnoreCase("8bit"))) {
          byte[] b = subject.getBytes();
          return new String(b, "gbk");
        }
        if (subject.indexOf("=?x-unknown?") >= 0) {
          subject = subject.replace("x-unknown", "gbk");
        }
        return MimeUtility.decodeText(subject);
      }
      return "无法获取主题";
    }
    catch (Exception ex){
    }
    return subject;
  }

  public static boolean parseMailName(String name) {
    if ((name != null) && (name.matches("^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$"))) {
      return true;
    }
    return false;
  }

  public static String getMessageFileName(String filename) {
    try {
      return MimeUtility.decodeText(filename);
    } catch (Exception ex) {
      //log.error(ex.getMessage());
    }
    return filename;
  }

  public static String parseFileType(String fileType) {
    if (fileType.equalsIgnoreCase("image/jpeg"))
      return "jpg";
    if (fileType.equalsIgnoreCase("image/png"))
      return "png";
    if (fileType.equalsIgnoreCase("image/gif")) {
      return "gif";
    }
    return "txt";
  }

  public static void outputMessage(Message msg) {
    try {
      String encoding = msg.getHeader("Content-Transfer-Encoding")[0];
      "8bit".equalsIgnoreCase(encoding);
    }
    catch (Exception localException1)
    {
    }

    ByteArrayOutputStream out = new ByteArrayOutputStream();
    try {
      msg.writeTo(out);
    } catch (Exception ex) {
      //log.error(ex.getMessage());
    }
  }
}
