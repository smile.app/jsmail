package com.smile.cn.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "EMailServer")
public class EMailServer {

	@XmlAttribute(name = "mailname", required = true)
	@XmlSchemaType(name = "anySimpleType")
	protected String mailname;

	@XmlAttribute(name = "mailtype", required = true)
	protected String mailtype;

	@XmlAttribute(name = "smtpserver", required = true)
	@XmlSchemaType(name = "anySimpleType")
	protected String smtpserver;

	@XmlAttribute(name = "smtpport", required = true)
	protected int smtpport;

	@XmlAttribute(name = "popserver", required = true)
	@XmlSchemaType(name = "anySimpleType")
	protected String popserver;

	@XmlAttribute(name = "popport", required = true)
	protected int popport;

	@XmlAttribute(name = "imapserver", required = true)
	@XmlSchemaType(name = "anySimpleType")
	protected String imapserver;

	@XmlAttribute(name = "imapport", required = true)
	protected int imapport;

	@XmlAttribute(name = "issmtpauth", required = true)
	protected boolean issmtpauth;

	@XmlAttribute(name = "securityprotocol", required = true)
	protected String securityprotocol;

	public String getMailname() {
		return this.mailname;
	}

	public void setMailname(String value) {
		this.mailname = value;
	}

	public String getMailtype() {
		return this.mailtype;
	}

	public void setMailtype(String value) {
		this.mailtype = value;
	}

	public String getSmtpserver() {
		return this.smtpserver;
	}

	public void setSmtpserver(String value) {
		this.smtpserver = value;
	}

	public int getSmtpport() {
		return this.smtpport;
	}

	public void setSmtpport(int value) {
		this.smtpport = value;
	}

	public String getPopserver() {
		return this.popserver;
	}

	public void setPopserver(String value) {
		this.popserver = value;
	}

	public int getPopport() {
		return this.popport;
	}

	public void setPopport(int value) {
		this.popport = value;
	}

	public int getImapport() {
		return this.imapport;
	}

	public void setImapport(int value) {
		this.imapport = value;
	}

	public String getImapserver() {
		return this.imapserver;
	}

	public void setImapserver(String value) {
		this.imapserver = value;
	}

	public boolean isIssmtpauth() {
		return this.issmtpauth;
	}

	public void setIssmtpauth(boolean value) {
		this.issmtpauth = value;
	}

	public String getSecurityprotocol() {
		return this.securityprotocol;
	}

	public void setSecurityprotocol(String value) {
		this.securityprotocol = value;
	}
}
