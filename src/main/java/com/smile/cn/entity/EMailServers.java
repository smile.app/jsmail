package com.smile.cn.entity;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "eMailServer" })
@XmlRootElement(name = "EMailServers")
public class EMailServers {

	@XmlElement(name = "EMailServer", required = true)
	protected List<EMailServer> eMailServer;

	public List<EMailServer> getEMailServer() {
		if (this.eMailServer == null) {
			this.eMailServer = new ArrayList<EMailServer>();
		}
		return this.eMailServer;
	}
}
