package com.smile.cn.entity;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class MailAuthentication extends Authenticator {
	private String strUser;
	private String strPwd;
	private String accountType;

	public MailAuthentication() {
	}

	public MailAuthentication(String user, String password) {
		this.strUser = user.trim();
		this.strPwd = password.trim();
	}

	public String getAccountType() {
		return this.accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getUserName() {
		return this.strUser;
	}

	public String getPassword() {
		return this.strPwd;
	}

	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(this.strUser, this.strPwd);
	}

	public String toString() {
		return this.strUser;
	}

	public void setStrUser(String strUser) {
		this.strUser = strUser;
	}

	public void setStrPwd(String strPwd) {
		this.strPwd = strPwd;
	}
}
