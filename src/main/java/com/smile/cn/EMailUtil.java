package com.smile.cn;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.smile.cn.bean.DeleteEmailBean;
import com.smile.cn.bean.ReceiveEmailBean;
import com.smile.cn.bean.SendEmailBean;
import com.smile.cn.entity.EMail;
import com.smile.cn.entity.MailAuthentication;

public class EMailUtil {
	/**
	 * send email
	 * 
	 * @param username
	 * @param password
	 * @param receivers
	 * @param ccs
	 *            the
	 * @param bccs
	 *            the
	 * @param title
	 * @param content
	 * @param attachs
	 * @param headers
	 */
	public static void sendEmail(String username, String password,
			List<String> receivers, List<String> ccs, List<String> bccs,
			String title, String content, List<String> attachs,
			Map<String, String> headers) {
		MailAuthentication account = new MailAuthentication(username, password);
		SendEmailBean send = new SendEmailBean();
		try {
			send.sendMail(account, receivers, ccs, bccs, title, content,
					attachs, headers);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * fetch email account
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public static int getAllEMailCount(String username, String password)
			throws Exception {
		MailAuthentication account = new MailAuthentication(username, password);
		ReceiveEmailBean receive = null;
		int amount = 0;
		try {
			receive = new ReceiveEmailBean(account);
			amount = receive.getAllEMailsCount();
		} catch (Exception e) {
			throw new Exception("get email amount failed:", e);
		}
		return amount;
	}

	/**
	 * fetch unread email amount
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public static int getUnReadEMailCount(String username, String password)
			throws Exception {
		MailAuthentication account = new MailAuthentication(username, password);
		ReceiveEmailBean receive = null;
		int amount = 0;
		try {
			receive = new ReceiveEmailBean(account);
			amount = receive.getUnreadEMailsCount();
		} catch (Exception e) {
			throw new Exception("get unread email amount failed:", e);
		}
		return amount;
	}

	/**
	 * fetch all emails
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public static List<EMail> getAllEMail(String username, String password)
			throws Exception {
		MailAuthentication account = new MailAuthentication(username, password);
		ReceiveEmailBean receive = null;
		List<EMail> mails = null;
		try {
			receive = new ReceiveEmailBean(account);
			mails = receive.getAllEMails();
		} catch (Exception e) {
			throw new Exception("get all email entity failed:", e);
		}
		return mails;
	}

	/**
	 * fetch unread emails
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public static List<EMail> getUnReadEMails(String username, String password)
			throws Exception {
		MailAuthentication account = new MailAuthentication(username, password);
		ReceiveEmailBean receive = null;
		List<EMail> mails = null;
		try {
			receive = new ReceiveEmailBean(account);
			mails = receive.getUnreaedEMails();
		} catch (Exception e) {
			throw new Exception("get unread email entity failed:", e);
		}
		return mails;
	}

	/**
	 * fetch email detail
	 * 
	 * @param username
	 * @param password
	 * @param number
	 * @return
	 * @throws Exception
	 */
	public static EMail getEMail(String username, String password, int number)
			throws Exception {
		MailAuthentication account = new MailAuthentication(username, password);
		ReceiveEmailBean receive = null;
		EMail mail = null;
		try {
			receive = new ReceiveEmailBean(account);
			mail = receive.getPerMail(number);
		} catch (Exception e) {
			throw new Exception("get singnal email entity failed:", e);
		}
		return mail;
	}

	/**
	 * fetch email under the start to end
	 * 
	 * @param username
	 * @param password
	 * @param start
	 * @param end
	 * @return
	 * @throws Exception
	 */
	public static List<EMail> getEMails(String username, String password,
			int start, int end) throws Exception {
		MailAuthentication account = new MailAuthentication(username, password);
		ReceiveEmailBean receive = null;
		List<EMail> mails = null;
		try {
			receive = new ReceiveEmailBean(account);
			mails = receive.getEMailByIndex(start, end);
		} catch (Exception e) {
			throw new Exception("get a round of emails failed:", e);
		}
		return mails;
	}

	/**
	 * query the emails
	 * 
	 * @param username
	 * @param password
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public static List<EMail> queryEMails(String username, String password,
			Map<String, Object> param) throws Exception {
		MailAuthentication account = new MailAuthentication(username, password);
		List<EMail> mails = new ArrayList<EMail>();
		try {
			ReceiveEmailBean receive = new ReceiveEmailBean(account);
			if ((param.isEmpty()) || (param == null)) {
				return getAllEMail(username, password);
			}
			String isread = (String) param.get("ISREAD");
			String addresser = (String) param.get("ADDRESSER");
			String subject = (String) param.get("SUBJECT");
			String content = (String) param.get("CONTENT");
			String min = (String) param.get("MINSIZE");
			String max = (String) param.get("MAXSIZE");
			int minSize = 0;
			int maxSize = 0;
			if ((min != null) && (!min.equalsIgnoreCase(""))) {
				minSize = Integer.parseInt(min);
			}
			if ((max != null) && (!max.equalsIgnoreCase(""))) {
				maxSize = Integer.parseInt(max);
			}
			Date mindate = (Date) param.get("MINDATE");
			Date maxdate = (Date) param.get("MAXDATE");
			mails = receive.getFactorEmails(isread, addresser, subject,
					content, minSize, maxSize, mindate, maxdate);
		} catch (Exception e) {
			throw new Exception("search emails failed:", e);
		}
		return mails;
	}

	/**
	 * delete the email by number for the pop3
	 * 
	 * @param username
	 * @param password
	 * @param number
	 * @return
	 * @throws Exception
	 */
	public static boolean deleteMail(String username, String password,
			int number) throws Exception {
		MailAuthentication account = new MailAuthentication(username, password);
		DeleteEmailBean delete = new DeleteEmailBean();
		try {
			delete.expungeMessage(account, number);
			return true;
		} catch (Exception e) {
			throw new Exception("delete email by number failed:", e);
		}
	}

	/**
	 * fetch the email by email id
	 * 
	 * @param username
	 * @param password
	 * @param uids
	 * @return
	 * @throws Exception
	 */
	public static List<EMail> getEMailByID(String username, String password,
			List<String> uids) throws Exception {
		MailAuthentication account = new MailAuthentication(username, password);
		ReceiveEmailBean receive = new ReceiveEmailBean(account);
		List<EMail> emails = receive.getEMailByUID(uids);
		return emails;
	}

	public static boolean deleteMailById(String username, String password,
			String id) throws Exception {
		MailAuthentication account = new MailAuthentication(username, password);
		DeleteEmailBean delete = new DeleteEmailBean();
		try {
			delete.expungeMessageByID(account, id);
			return true;
		} catch (Exception e) {
			throw new Exception(
					"delete email by id failed maybe the id not exists:", e);
		}
	}

	public static boolean emptyUserEmail(String username, String password)
			throws Exception {
		MailAuthentication account = new MailAuthentication(username, password);
		DeleteEmailBean delete = new DeleteEmailBean();
		try {
			delete.emptyFolder(account);
		} catch (Exception e) {
			throw new Exception("delete emails  failed :", e);
		}
		return true;
	}
}
